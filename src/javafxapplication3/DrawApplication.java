
package javafxapplication3;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 *
 * @author Ahmed
 */
public class DrawApplication extends Application {
    
    @Override
    public void start(Stage primaryStage) {
        Draw draw=new Draw();
       
        primaryStage.setTitle("Restaurant Reservation");
        draw.prepareScene();
      primaryStage.setScene(draw.getScene());
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
    
}
